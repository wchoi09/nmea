# GPS, GNSS, NMEA 데이터

## 1. 개요

## 2. 용어 정리

### 2.1. GPS

### 2.2. GNSS

### 2.3. NMEA

#### 2.3.1. GPGGA

```text
$GPGGA,024030,3725.3199,N,12205.0400,W,1,6,,5.0,M,0.,M,,,*47
```

| Idx | Field | Value | Comments |
|:---|:---|:---|:---|
| 1 | Sentence ID | $GPGGA | Global Positioning system fixed data |
| 2 | UTC Time | 024030 | Time (hhmmss) |
| 3 | Latitude | 3725.3199 | 위도 ddmm.mmmm 혁식 |
| 4 | N/S Indicator | N | N = North, S = South<br>북위/남위 |
| 5 | Longitude | 12205.0400 | 경도 dddmm.mmmm 형식 |
| 6 | E/W Indicator | W | E = East, W = West<br>동경/서경 |
| 7 | Position Fix | | |
| 8 | Satellites Used | | |
| 9 | HDOP | | |
|10 | Altitude | | |
| 11 | Altitude Units | | |
| 12 | Geoid Separation | | |
| 13 | Separation Units | | |
| 14 | DGPS Age | | |
| 15 | DPGS Station ID | | |
| 16 | Checksum | | |
| 17 | Terminator | | |

#### 2.3.2. GPRMC

```text
$GPRMC,024030,A,3725.3199,N,12205.0400,W,0.00,0.00,100523,0.0,W*47
```
