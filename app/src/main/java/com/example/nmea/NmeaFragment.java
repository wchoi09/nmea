package com.example.nmea;

import static java.lang.Math.floor;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.GnssStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.OnNmeaMessageListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.example.nmea.databinding.FragmentNmeaBinding;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

public class NmeaFragment extends Fragment implements OnNmeaMessageListener, LocationListener {

    public NmeaFragment() {}

    private static final String TAG = "NMEA";
    private FragmentNmeaBinding binding;
    private TextView nmeaRmcTv;
    private TextView nmeaGgaTv;
    private TextView nmeaOtherTv;
    private LocationManager mLocationManager;
    private Handler handler = new Handler(Looper.getMainLooper());

    private static final String[] PERMISSIONS = {
        android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION
    };
    private String lastNmeaMessage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentNmeaBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (!hasPermission()) {
            requestPermissionLauncher.launch(PERMISSIONS);
        }

        mLocationManager =
                (LocationManager) requireContext().getSystemService(Context.LOCATION_SERVICE);

        try {
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
            mLocationManager.registerGnssStatusCallback(mStatusCallback, handler);
            mLocationManager.addNmeaListener(this, handler);
            lastNmeaMessage = "";
        } catch (SecurityException e) {
            Log.w(TAG, "Unable to open GPS", e);
        }

        nmeaRmcTv = binding.tvNmeaGprmc;
        nmeaGgaTv = binding.tvNmeaGpgga;
        nmeaOtherTv = binding.tvNmeaOther;
    }

    private final ActivityResultLauncher<String[]> requestPermissionLauncher =
            registerForActivityResult(
                    new ActivityResultContracts.RequestMultiplePermissions(),
                    isGranted -> {
                        if (isGranted.containsValue(true)) {
                            Toast.makeText(
                                            requireContext(),
                                            "Permission request granted",
                                            Toast.LENGTH_LONG)
                                    .show();
                        } else {
                            Toast.makeText(
                                            requireContext(),
                                            "Permission request denied",
                                            Toast.LENGTH_LONG)
                                    .show();
                        }
                    });

    @Override
    public void onResume() {
        super.onResume();
        try {
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
            mLocationManager.registerGnssStatusCallback(mStatusCallback);
            mLocationManager.addNmeaListener(this);
            lastNmeaMessage = "";
        } catch (SecurityException e) {
            Log.w(TAG, "Unable to open GPS", e);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mLocationManager.removeUpdates(this);
        mLocationManager.unregisterGnssStatusCallback(mStatusCallback);
        mLocationManager.removeNmeaListener(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
        mLocationManager.removeUpdates(this);
        mLocationManager.unregisterGnssStatusCallback(mStatusCallback);
        mLocationManager.removeNmeaListener(this);
    }

    private final GnssStatus.Callback mStatusCallback =
            new GnssStatus.Callback() {
                @Override
                public void onStarted() {}

                @Override
                public void onStopped() {}

                @Override
                public void onFirstFix(int ttffMillis) {}

                @Override
                public void onSatelliteStatusChanged(GnssStatus status) {
                    Log.v(
                            TAG,
                            String.format("Number of satellites: %d", status.getSatelliteCount()));
                }
            };

    /** Report raw NMEA messages */
    @Override
    public void onNmeaMessage(String message, long timestamp) {
        lastNmeaMessage = message;
        List<String> nmeaList = Arrays.asList(lastNmeaMessage.split("[,*]"));
        // Log.v(TAG, String.format("NMEA: %s", lastNmeaMessage));
        if (lastNmeaMessage.startsWith("GPGGA", 1)) {
            nmeaGgaTv.setText(lastNmeaMessage);
            // logGga(nmeaList);

        } else if (lastNmeaMessage.startsWith("GPRMC", 1)) {
            nmeaRmcTv.setText(lastNmeaMessage);
            Log.v(TAG, String.format("NMEA: %s", lastNmeaMessage));
            logRmc(nmeaList);
        } else {
            nmeaOtherTv.setText(lastNmeaMessage);
        }
    }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        Log.v(TAG, String.format("Location update: %s", location));
        System.out.println("PARSED USING LOCATION LISTENER");
        Log.v(TAG, String.format("LATITUDE: %f", location.getLatitude()));
        Log.v(TAG, String.format("LONGITUDE: %f", location.getLongitude()));
    }

    private void logGga(List<String> ggaLst) {
        Log.v(TAG, String.format("ID: %s", ggaLst.get(0)));
        // Log.v(TAG, String.format("UTC TIME: %s", ggaLst.get(1)));
        // Log.v(TAG, String.format("LATITUDE: %s", ggaLst.get(2)));
        // Log.v(TAG, String.format("N/S: %s", ggaLst.get(3)));
        // Log.v(TAG, String.format("LONGITUDE: %s", ggaLst.get(4)));
        // Log.v(TAG, String.format("E/W: %s", ggaLst.get(5)));
        logLatLong(ggaLst.get(3), ggaLst.get(2), ggaLst.get(5), ggaLst.get(4));
        // Log.v(TAG, String.format("POSITION FIX: %s", ggaLst.get(6)));
        Log.v(TAG, String.format("SATELLITES USED: %s", ggaLst.get(7)));
        // Log.v(TAG, String.format("HDOP: %s", ggaLst.get(8)));
        // Log.v(TAG, String.format("ALTITUDE: %s", ggaLst.get(9)));
        // Log.v(TAG, String.format("ALTITUDE UNITS: %s", ggaLst.get(10)));
        // Log.v(TAG, String.format("GEOID SEPARATION: %s", ggaLst.get(11)));
        // Log.v(TAG, String.format("SEPARATION UNITS: %s", ggaLst.get(12)));
        // Log.v(TAG, String.format("DGPS AGE: %s", ggaLst.get(13)));
        // Log.v(TAG, String.format("DPGS STATION ID: %s", ggaLst.get(14)));
        // Log.v(TAG, String.format("똥: %s", ggaLst.get(15)));
        Log.v(TAG, String.format("CHECKSUM: %s", ggaLst.get(16)));
    }

    private void logRmc(List<String> rmcLst) {
        /*Log.v(TAG, String.format("ID: %s", rmcLst.get(0)));
        Log.v(TAG, String.format("UTC TIME: %s", rmcLst.get(1)));
        Log.v(TAG, String.format("STATUS: %s", rmcLst.get(2)));
        // Log.v(TAG, String.format("LATITUDE: %s", rmcLst.get(3)));
        // Log.v(TAG, String.format("N/S: %s", rmcLst.get(4)));
        // Log.v(TAG, String.format("LONGITUDE: %s", rmcLst.get(5)));
        // Log.v(TAG, String.format("E/W: %s", rmcLst.get(6)));
        logLatLong(rmcLst.get(4), rmcLst.get(3), rmcLst.get(6), rmcLst.get(5));
        // Log.v(TAG, String.format("SPEED OVER GROUND: %s", rmcLst.get(7)));
        // Log.v(TAG, String.format("COURSE OVER GROUND: %s", rmcLst.get(8)));
        Log.v(TAG, String.format("UTC DATE: %s", rmcLst.get(9)));
        printDateTime(rmcLst.get(9), rmcLst.get(1));
        // Log.v(TAG, String.format("MAGNETIC VAR.: %s", rmcLst.get(10)));
        Log.v(TAG, String.format("W/S: %s", rmcLst.get(11)));
        Log.v(TAG, String.format("CHECKSUM: %s", rmcLst.get(12)));*/
        System.out.println("PARSED USING NMEA LISTENER");
        logLatLong(rmcLst.get(4), rmcLst.get(3), rmcLst.get(6), rmcLst.get(5));
        printDateTime(rmcLst.get(9), rmcLst.get(1));
    }

    private void printDateTime(String dateStr, String timeStr) {
        // Convert date and time strings to LocalDateTime object
        String dateTimeStr = dateStr + timeStr;
        LocalDateTime dateTime =
                LocalDateTime.parse(dateTimeStr, DateTimeFormatter.ofPattern("ddMMyyHHmmss"));

        ZonedDateTime utcZonedDateTime = ZonedDateTime.of(dateTime, ZoneId.of("UTC"));

        // Convert to KST timezone
        ZoneId kstZoneId = ZoneId.of("Asia/Seoul");
        ZonedDateTime kstZonedDateTime = utcZonedDateTime.withZoneSameInstant(kstZoneId);

        // Convert back to LocalDateTime
        LocalDateTime kstDateTime = kstZonedDateTime.toLocalDateTime();

        // Format date-time string
        String formattedKstZonedDateTime =
                kstDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));

        String formattedUtcZonedDateTime =
                utcZonedDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));

        Log.v(TAG, String.format("UTC: %s", formattedUtcZonedDateTime));
        Log.v(TAG, String.format("KST: %s", formattedKstZonedDateTime));
    }

    private void logLatLong(
            String latDirStr, String latitudeStr, String longDirStr, String longitudeStr) {
        float latDeg;
        float longDeg;

        latDeg = Float.parseFloat(latitudeStr);
        longDeg = Float.parseFloat(longitudeStr);

        latDeg = (float) (floor(latDeg / 100) + (latDeg % 100) / 60.0f);
        longDeg = (float) (floor(longDeg / 100) + (longDeg % 100) / 60.0f);

        if (latDirStr.equals("S")) {
            latDeg *= -1;
        }
        if (longDirStr.equals("W")) {
            longDeg *= -1;
        }

        Log.v(TAG, String.format("LATITUDE: %s", latDeg));
        Log.v(TAG, String.format("LONGITUDE: %s", longDeg));
    }

    // Check for permissions
    private boolean hasPermission() {
        for (String permission : PERMISSIONS) {
            if (ContextCompat.checkSelfPermission(requireContext(), permission)
                    != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }
}
